import { NgModule } from '@angular/core';
import { HttpCacheComponent } from './http-cache.component';



@NgModule({
  declarations: [HttpCacheComponent],
  imports: [
  ],
  exports: [HttpCacheComponent]
})
export class HttpCacheModule { }
